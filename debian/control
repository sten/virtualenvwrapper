Source: virtualenvwrapper
Section: python
Priority: optional
Maintainer: Jan Dittberner <jandd@debian.org>
Build-Depends: debhelper (>= 11), dh-python
Build-Depends-Indep: bash-completion,
                     python-all,
                     python3-all,
                     python-pbr, python3-pbr,
                     python-setuptools (>= 0.6b3),
                     python3-setuptools (>= 0.6b3),
                     python-sphinx (>= 1.0.7+dfsg) | python3-sphinx
Standards-Version: 4.1.4.1
Homepage: https://virtualenvwrapper.readthedocs.io/en/latest/
Vcs-Git: https://salsa.debian.org/debian/virtualenvwrapper.git
Vcs-Browser: https://salsa.debian.org/debian/virtualenvwrapper
Testsuite: autopkgtest-pkg-python

Package: python-virtualenvwrapper
Architecture: all
Depends: python-virtualenv,
         ${misc:Depends},
         ${python:Depends}
Recommends: virtualenvwrapper
Description: extension to virtualenv for managing multiple environments (Py2)
 virtualenvwrapper is a set of extensions to Ian Bicking's virtualenv
 tool. The extensions include wrappers for creating and deleting
 virtual environments and otherwise managing your development
 workflow, making it easier to work on more than one project at a time
 without introducing conflicts in their dependencies.
 .
 This package installs the library for Python 2.

Package: python3-virtualenvwrapper
Architecture: all
Depends: python3-virtualenv,
         ${misc:Depends},
         ${python3:Depends}
Recommends: virtualenvwrapper
Description: extension to virtualenv for managing multiple environments (Py3)
 virtualenvwrapper is a set of extensions to Ian Bicking's virtualenv
 tool. The extensions include wrappers for creating and deleting
 virtual environments and otherwise managing your development
 workflow, making it easier to work on more than one project at a time
 without introducing conflicts in their dependencies.
 .
 This package installs the library for Python 3.

Package: virtualenvwrapper
Architecture: all
Depends: python-virtualenvwrapper | python3-virtualenvwrapper,
         ${misc:Depends},
Recommends: bash-completion
Suggests: virtualenvwrapper-doc
Description: extension to virtualenv for managing multiple environments
 virtualenvwrapper is a set of extensions to Ian Bicking's virtualenv
 tool. The extensions include wrappers for creating and deleting
 virtual environments and otherwise managing your development
 workflow, making it easier to work on more than one project at a time
 without introducing conflicts in their dependencies.

Package: virtualenvwrapper-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: extension to virtualenv for managing multiple environments (docs)
 virtualenvwrapper is a set of extensions to Ian Bicking's virtualenv
 tool. The extensions include wrappers for creating and deleting
 virtual environments and otherwise managing your development
 workflow, making it easier to work on more than one project at a time
 without introducing conflicts in their dependencies.
 .
 This is the common documentation package.
